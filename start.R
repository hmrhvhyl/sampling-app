# This file install packages required for the sampling dashboard and set path to some folders.

## Log File
logProdPath <- 'W:/Production/Estimation/Sampling/log files'  #estimation
logTestPath <- 'W:/Testing/Estimation/Sampling/log files'   # SM

logPath <- if(file.access(logProdPath)==0){
  logProdPath
}else if (file.access(logTestPath)==0){
  logTestPath
}else{getwd()}                                          # other 

# create year and month folder if doesn't exist
logPathY <- file.path(logPath, format(Sys.Date(), "%Y"))  
dir.create(logPathY, showWarnings = FALSE)
logPathM <- file.path(logPathY, format(Sys.Date(), "%m"))
dir.create(logPathM, showWarnings = FALSE)

# set time
Sys.setenv(TZ= "America/New_York")
ID <- as.integer(Sys.time())
originName <- sprintf('General(%s)_%s.txt',ID,format(Sys.time(),"%Y-%m-%d_%H-%M-%S"))

# generate log file
fid <- file(paste(logPathM, originName, sep = "/"), open = "wt")
sink(fid, type = "message", split = F)
sink(fid, append = T, type = "output", split = T)

# ## Setup
# # .RProfile contents
# conf <- c('options(repos = "https://f3eaipitcat12.statcan.ca:8443/artifactory/cran")', 'options(pkgType = "binary")')
# 
# # Write .RProfile if it doesn't exist already
# if (!file.exists(file.path('~', '.RProfile'))) {
#   writeLines(conf, file.path('~', '.RProfile'))
# }
# 
# # Load new .RPofile
# source(file.path("~", ".RProfile"))

## All Packages need
shinyPackage <- c("shiny", "shinydashboard", "htmltools", "DT", "yaml", "shinyBS")
programPackage <- c("RODBC", "dplyr", "scales", "ggplot2", "stringr", "readxl", "haven", "rstudioapi", "plotly", "treemap", 
                    "stratification",'stringr',"tools")
rmarkdownPackage <- c("evaluate", "highr", "markdown", "caTools", "bitops", "knitr", "base64enc", "rprojroot", "rmarkdown")

allPackages <- c(shinyPackage, programPackage, rmarkdownPackage)

## Install
needInstall <- allPackages[!allPackages %in% installed.packages()[, 1]]
if (length(needInstall) > 0) {
  install.packages(pkgs = needInstall)
}

## Open Library
library(shiny)
library(shinydashboard)
library(htmltools)

## Set source
IEPath <- "T:\\Data Exchange\\Estimation\\Estimation-BSMD\\RShiny\\Importer Registry"
ASMLFile <- "sales_frame_splits_dest.sas7bdat"
ASMLPath <- "T:\\Data Exchange\\Estimation\\Estimation-BSMD/RShiny\\ASML\\Output"
ASMLFull <- paste(ASMLPath,ASMLFile, sep = "\\")


