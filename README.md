*GitLab doesn't play nice with Internet Explorer; please use Firefox or Chrome instead.*

# PPD Sampling Application

## Background

From 2019, estimation is interested in taking up the sampling activities for PPD indices. The goal is to make sampling more streamlined so that companies can be sampled more often for better representativeness of the current market, and the budget spent on sampling can be reduced.

Across 30 indices in PPD, 13 of them can be sampled by this application. 

|Program Acr | Program Name |
|---         |---       |
|AESPI       | Architecture and Engineering     |
|ASPI        | Accounting     |
|CIMERLSPI   | Machinery and Equipment Rental     |
|CMSPI       | Couriers and Messengers     |
|COSPI       | Consulting     |
|CRSPI       | Commercial Rents     |
|EIPI        | Export-Import     |
|FHMCFSPI    | Trucking     |
|IPPI/RMPI   | Industrial Property/ Raw Material     |
|IPSPI       | Informatics and Professional Services     |
|PRSPI       | Passenger Rail     |
|RSPI        | Retail     |
|WSPI        | Wholesale     |


For more information on indices' details, please go to 
[Survey Life Cycle](https://gcdocs.gc.ca/statcan/llisapi.dll?func=ll&objaction=overview&objid=5641912). Previous sample summaries are available in [W Drive](W:\Production\Estimation\Sampling) (Data File) and 
[GCDOCs](https://gcdocs.gc.ca/statcan/llisapi.dll?func=ll&objId=4923420&objAction=browse) (Documentation)


## Sampling Design

1. Stratification: Except CRSPI which uses Lavall?e-Hidiroglou stratification, almost all indices include some sort on industry and/or geography as stratification. This stratification is dictated by subject matter constraints and can also be considered as domains of interest.

2. Allocation: Except CRSPI which uses power allocation, almost all indices assign sample size to each group proportional to its size. Moreover, a minimal/judgemental sample size can be set in the application to keep the generalization of the sampling.

3. Cut-off: As is the case in most business surveys, the distribution of revenues in the target population is highly skewed. Therefore, interviewing all kinds of companies, regardless of their revenues, may yield responses/quantities that are extremely irrelevant for the market as a whole (in other words, for the estimates). Additionally, interviewing small companies (with a very small revenue) is deemed as an unacceptable burden on small enterprises. As a consequence, all PPD samples perform a "cut-off" of the frame, based on revenues, before selecting the sample. Subject matter experts decide what proportion of the market they want the survey to represent; this determines the cut-off threshold, to cut the smallest, say, 5% or 10% of the revenue.

#### Top-up/Cut-off

The Top-up design is applied when top companies contribute most revenue (e.g. the largest 10 units account for at least 90% of revenue for that frame.

All frame units were placed in descending order of revenue. Units were selected until the desired number of units were selected or the desired percent of revenue coverage had been reached. The design weight will be 1 for each unit.


#### Probability Proportional to Size (PPS)

Most indices use PPS design. There are two main reasons to use selection probabilities proportional to revenues. 

Firstly, companies with larger revenues have a larger share of the market and hence their
prices must be highly representative of the current market conditions. It is important to assure, as much as possible, that the companies whose prices resemble most of the current market conditions are included in the sample. 

Secondly, to the extent that prices (actually, price relatives) are associated with revenues, using a scheme proportional to revenues will tend to reduce the sampling variability of the estimates.

This means that the CVs of the produced indices will be reduced. In summary, the "algorithm" of this kind of design is as follows:

1. For each stratum, remove from the frame the smallest companies until the revenue left in the frame is, say, 90% or 95% of the original stratum's total revenue.
2. Allocate the sample to each group. For the current designs, this allocation is either proportional or proportional to revenue. Minimum stratum sample sizes can also be used; the rest is allocated as before.
3. In each stratum select the sample by systematic PPS. Multiple iterations may be required, removing from the frame the necessary must-take units (for which the
selection probabilities are larger than one). In each iteration, the removed
companies are included in the sample with certainty.
4. Calculate the sampling weight as the inverse of the selection probabilities, or equal
to one for the must-takes.

#### Simple Random Sample (SRS)

When price movements are uncorrelated with revenue, selecting the sample with a PPS design does not
reduce the sampling variability (and it may even increase it). Moreover, there is nonresponse to initialization and attrition in many surveys. It is desirable to have a sample design that permits easy replacement of sample units to balance the effects of nonresponse and attrition. 

With an SRS design, it is easy to replace units, since all replacement units would have the same chance of selection and weight as the replaced units. Whereas under a PPS design, units have unequal weights (proportional to revenue), making sample replacement more difficult.

The goal of ensuring the inclusion of the biggest contributors to the market still holds, but a
PPS design is not required. This can still be accomplished by having Take-All strata under a
stratified SRS design.

<!-- ### Judgemental -->

## Process Flow

**SMs** would visualize data frame summary, analyze sampling design, 
and draw preliminary samples guided by  [Sampling Dashboard Documentation](https://gcdocs.gc.ca/statcan/llisapi.dll?func=ll&objaction=overview&objid=5806136).

**Estimation** draws the final sample.

**ESMD** could be involved for the redesigns, if the target population, frame, strata and domains of interest, measure-of-size, cut-off threshold, precision requirements, sample size, and allocation strategy change.

The below diagram indicates the sampling workflow between SMs, Estimation and ESMD.

![](documents/img/Process.PNG)


## Maintenance (Estimation Only)

Please follow these documentations for bug fixing and improvement.

1. [Maintenance Documentation](https://gcdocs.gc.ca/statcan/llisapi.dll?func=ll&objaction=overview&objid=6907908)

2. [Program Structure](https://gcdocs.gc.ca/statcan/llisapi.dll?func=ll&objaction=overview&objid=6904227)

3. [GitLab Documentation](https://gcdocs.gc.ca/statcan/llisapi.dll?func=ll&objaction=overview&objid=8149649)




