## Program: a_source_IERegistry.R	
## Author: Yuetong Liu							 
## Date: July, 2019	

## Purpose: 
## This program builds data frames from Import-Export Registry

## Use: 
## Use the following sourcw statement in the sampling program.                                   
# Version in gitlab: 
# source("https://f3eaipitcap01.statcan.ca/estimation/sampling_shiny_application/raw/master/Program/a_source_IERegistry.R", echo = TRUE, max.deparse.length = Inf)
# Version in W:
# source(file = "W:/Development/Estimation/Sampling/Dashboard/PreliminaryProgram/Program/a_source_IERegistry.R")

## The function call is 
# GetFrameIE (IEHSCode, IECountries, raw.data.file)
## "IEHSCode" is a list of NAICS
## "IECountries" is a list of countries
## "raw.data.file" is the raw data frame from IE registry
## This function returns a data frame table derived from IE Registry

## Open package
library(haven)
library(dplyr)

##function
GetFrameIE <- function(IEHSCode, IECountries, raw.data.file){
  
  IE.raw <- raw.data.file%>%
    rename(HS10Code=HS10_CODE,
           HS10Description=HS10desc,
           businessNumber=BN,
           legalName=LEGAL_NAME,
           numberOfTransactions=No_transactions)%>%
    filter(businessNumber != "")
  
  IE.frame <- IE.raw%>%
    mutate(HS6Code = substring(HS10Code, 0,6))%>%
    filter(HS6Code %in% IEHSCode,
           country %in% IECountries)%>%
    arrange(HS6Code,country,businessNumber)
  
  
  IE.frame.rev <- IE.frame%>%
    group_by(HS6Code,country,businessNumber)%>%
    summarise(countEstablishement = n(),
              revenueBN = sum(value))%>%
    mutate(complex = case_when(countEstablishement ==1 ~ 0,
                               TRUE ~ 1))
  return(IE.frame.rev)
}

