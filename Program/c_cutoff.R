## Program: c_curoff.R	
## Author: Yuetong Liu							 
## Date: July, 2019	

## Purpose: 
## This program used the "CutOff" method to draw samples in a data frame. 
## There are three functions in this file.
## 1. "CutOffSample" draws samples using the cutoff method
## 2. "SampleSummary" compares total revenue and # units of the sample and the data frame
## 3. "CutOffAttributes" gets detailed units' information from BRS
                                       
## Use: 
## Use the following source statement in the sampling program.                                   
# Version in gitlab: 
# source("https://f3eaipitcap01.statcan.ca/estimation/sampling_shiny_application/raw/master/Program/c_curoff.R", echo = TRUE, max.deparse.length = Inf)
# Version in W:
# source(file = "W:/Development/Estimation/Sampling/Dashboard/PreliminaryProgram/Program/c_curoff.R")

## The function calls are 
## 1. CutOffSample(data.frame, allocation.summary, strataVar, revenueVar) 
## "data.frame" is the data frame table used for sampling.
## "allocation.summary" is the table shows sample size allocated in each strata.
## "strataVar" is the name of one variable in 'frame'.
## "revenueVar" is the name of one  variable in 'dataFrame'.
## This function returns a sample table.

## 2. 
# SampleSummary(sample.frame, allocation.summary, strataVar, revenueVar) 
## "sample.frame" is the table returned in "CutOffSample" function.
## "allocation.summary" is the table shows sample size allocated in each strata.
## "strataVar" is the name of one variable in 'sample.frame'.
## "revenueVar" is the name of one  variable in 'dataFrame'.
## This function returns a table contains total revenue and # units of the sample and the data frame in each strata.

## 3. 
# CutOffAttributes(sample.frame, sufdate) 
## "sample.frame" is the table returned in "CutOffSample" function.
## "sufdate" is the reference month for tblSUFGeneric table in BRS.
## This function returns the sample table with detailed units' information from BRS.

## Open package
library(RODBC) 
library(dplyr)
library(scales)

## 1. draw sample function
CutOffSample <- function(data.frame, allocation.summary, strataVar, revenueVar){
  
  ## Frame to be sampled
  cutOff.frame <- merge(data.frame,allocation.summary, by = strataVar) %>%
    arrange(!!(as.name(strataVar)), desc(!!(as.name(revenueVar)))) %>%
    mutate(nn=0)
  
  
  ## Build empty sample frame
  sample.frame <- cutOff.frame[0,]
  
  ## Draw Sample
  i <- 1
  indicator <- eval(parse(text = paste0("cutOff.frame$",strataVar, sep = "")))
  while(i <= nrow(cutOff.frame)){
    if(i == 1||indicator[i-1] != indicator[i]){
      cutOff.frame$nn[i]=1
    }
    else{cutOff.frame$nn[i]=cutOff.frame$nn[i-1]+1}
    if(cutOff.frame$nn[i]<=cutOff.frame$sampleSize[i]){
      sample.frame<-rbind(sample.frame,cutOff.frame[i,])
    }
    i=i+1
  }
  
  return(sample.frame)
}

## 2.Sample summary function 
SampleSummary <- function(sample.frame, allocation.summary, strataVar, revenueVar){
  sample.revenue <- sample.frame %>%
    group_by(!!(as.name(strataVar))) %>%
    summarise(sampleRevenue=sum(!!(as.name(revenueVar))))
  sample.summary <- merge(sample.revenue,allocation.summary, by = strataVar, all.y = TRUE)%>%
    mutate(sampleRevenue = case_when(is.na(sampleRevenue) ~ 0, 
                                       TRUE ~ sampleRevenue),
           revenueCoveragePercentage=percent(sampleRevenue/strataRevenue))
  
  return(sample.summary)
}

# sample <-  read.csv("W:/Development/Estimation/Sampling/Dashboard/Input/downloadCutOffSample.csv")
# sufdate=201905

## 3. Select Interesting Variables
CutOffAttributes <- function(sample.frame, sufdate){
  
  gen.SUF <- odbcDriverConnect(
    "driver={SQL Server};server=BRDSQLSIRPROD;database=SurveyInterfaceRepository; trusted_connection=yes" #server and database(catalog)
  )
  
  i <- 1
  while (i < nrow(sample.frame)) {
    if(i+1000 > nrow(sample.frame)){OEN <- toString(paste("'",sample.frame$OperatingEntityNumber[i:nrow(sample.frame)],"'",sep=""))}else{
      j <- i+1000
      OEN <- toString(paste("'",sample.frame$OperatingEntityNumber[i:j],"'",sep=""))}
    
    sql =sprintf("SELECT OperatingEntityNumber,
                 OPAddressCity,
                 OperatingNameEng,
                 OperatingNameFr,
                 LegalNameEng,
                 LegalNameFr
                 from %s
                 where OperatingEntityNumber in (%s)",paste("tblSUFGeneric",sufdate, sep=""),OEN)
    
    sample.attribute.1 <- sqlQuery(gen.SUF,sql)
    if(i == 1){sample.attribute <- sample.attribute.1}else{
      sample.attribute = rbind(sample.attribute,sample.attribute.1)
    }
    i = i+1001
  }
  sample.final<-merge(sample.frame,sample.attribute, all.x=TRUE)
  
  return(sample.final)
}



