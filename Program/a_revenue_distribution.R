## Program: a_revenue_distribution.R	
## Author: Yuetong Liu							 
## Date: July, 2019	

## Purpose: 
## This program calculates total revenue and revenue distribution in a data frame. 
## There are four functions in this files.
## 1. "TotalRevenue" calculates total revenue in each strata
## 2. "RevenueShare" assigns 'revshare' and 'cumshare' to each unit
## 3. "RevenueDistributionSummary" gets revenue distribution in each strata
## 4. "RevenueDistributionSummaryTotal" get total revenue distrbution 
## 5. "CutStrata" get a list of strata which have less than 10 units/10 units contribution to more than 90% strata revenue

## Use: 
## Use the following source statement in the sampling program.                                   
# Version in gitlab: 
# source("https://f3eaipitcap01.statcan.ca/estimation/sampling_shiny_application/raw/master/Program/a_revenue_distribution.R", echo = TRUE, max.deparse.length = Inf)
# Version in W:
# source(file = "W:/Development/Estimation/Sampling/Dashboard/PreliminaryProgram/Program/a_revenue_distribution.R")

## The function calls are 
## 1. TotalRevenue <- function(data.frame, strataVar, revenueVar)
## "data.frame" is the data frame table 
## "strataVar" is the name of one  variable in 'data.frame'.
## "revenueVar" is the name of one  variable in 'data.frame'.
## This function returns a table contains # units and total revenue in each strata.
    
## 2. RevenueShare <- function(data.frame,revenue.summary,strataVar,revenueVar)
## "data.frame" is the data frame table, 
## "revenue.summary" is the table returned in "TotalRevenue" function. 
## "strataVar" is the name of one  variable in 'data.frame'.
## "revenueVar" is the name of one  variable in 'data.frame'.
## This function returns the 'data.frame' table with two extra variables: "revshare" and "cumshare".

## 3. RevenueDistributionSummary <- function(rev.share,strataVar,revenueVar)
## "rev.share" is the table returned in "RevenueShare" function. 
## "strataVar" is the name of one  variable in 'data.frame'.
## "revenueVar" is the name of one  variable in 'data.frame'.
## This function returns a table with revenue distribution in each strata.

## 4. 
# RevenueDistributionSummaryTotal(data.frame,revenueVar,revenue.summary)
## "rev.share" is the table returned in "RevenueShare" function. 
## "revenueVar" is the name of one  variable in 'data.frame'.
## This function returns a table with total revenue distribution of the "data.frame".

## 5. 
# CutStrata(strataVar, revenue, revenue.summary, rev.share)
## "strataVar" is the name of one  variable in 'rev.share'.
## "revenue" is the name of one  variable in 'data.frame'.
## "revenue.summary" is the table returned in "TotalRevenue" function. 
## "rev.share" is the table returned in "RevenueShare" function. 
## This function returns a list with strata in the "rev.share".

## Open Package
library(RODBC) 
library(dplyr)
library(scales)
library(stringr)


## 1. Total revenue table function


TotalRevenue <- function(data.frame, strataVar, revenueVar){
  
  strata.revenue <- data.frame%>%
    group_by(!!(as.name(strataVar)))%>%
    summarise(strataCount = n(),
              strataRevenue = as.numeric(sum(!!(as.name(revenueVar)), na.rm = TRUE))
    )
  
  #Total
  total.revenue = data.frame%>%
    ungroup(!!(as.name(strataVar)))%>%
    summarise(totalCount = n(),
              totalRevenue = as.numeric(sum(!!(as.name(revenueVar)), na.rm = TRUE))
    )
 
  
  #Merge
  revenue.summary <- merge(strata.revenue,total.revenue,by = NULL)
  
  return(revenue.summary)
}

## 2. Revenue share table function
RevenueShare <- function(data.frame,revenue.summary,strataVar,revenueVar){
  
  ## Revenue share
  rev.share <- merge(data.frame,revenue.summary)%>%
    arrange(!!(as.name(strataVar)),!!(as.name(revenueVar)))%>%
    mutate(revshare = !!(as.name(revenueVar))/strataRevenue)%>% # Revenue share
    group_by(!!(as.name(strataVar)))%>%
    mutate(cumshare = cumsum(revshare)) ## Cumshare

  return(rev.share)}


## 3. Revenue Distribution Summary Function
RevenueDistributionSummary <- function(rev.share,strataVar,revenueVar){
  
  distributionList <- paste0(seq(5,100,by=5),"%")
  
  distribution.table <- rev.share%>%
    
    arrange(!!(as.name(strataVar)),!!(as.name(revenueVar)))%>%
    group_by(!!(as.name(strataVar)))%>%
    mutate(interval = cut(cumshare,b = 20, labels = distributionList)
           # ,
           # interval = paste(lag(interval),interval,sep='~'),
           # interval = str_replace(interval,'NA','0%')
           )%>% # Interval
    
    group_by(!!(as.name(strataVar)),interval)%>%
    summarise(topRevenueInInterval = max(!!(as.name(revenueVar))), # top Revenue In Interval
              numberOfUnitsInInterval = n())%>%  # # Units In Interval
  
      mutate(cumBottom = cumsum(numberOfUnitsInInterval))%>% # CumulativeFromBottom
      arrange(desc(as.numeric(sub("\\%.*", "", interval))))%>%
      mutate(cumSum = cumsum(numberOfUnitsInInterval))%>% # CumulativeFromTop
      arrange(as.numeric(sub("\\%.*", "", interval)))
  
  
  return(distribution.table)}

## 4 Revenue Distribution Summary Function Total

RevenueDistributionSummaryTotal <- function(data.frame,revenueVar,revenue.summary){
  
  ## Revenueshare, Cumshare total
  rev.share.total <- merge(data.frame,revenue.summary)%>%
    arrange(!!(as.name(revenueVar)))%>%
    mutate(revshare = !!(as.name(revenueVar))/totalRevenue,
           cumshare = cumsum(revshare))
  
  ## # units in interval
  distributionList <- paste0(seq(5,100,by=5),"%")
  
  
  
  distribution.table <- rev.share.total%>%
    arrange(!!(as.name(revenueVar)))%>%
    mutate(interval = cut(cumshare,breaks = 20, labels = distributionList)
           # ,
           # interval = paste(lag(interval),interval,sep='~'),
           # interval = str_replace(interval,'NA','0%')
           )%>% # Interval
    group_by(interval)%>%
    summarise(topRevenueInInterval = max(!!(as.name(revenueVar))), # top Revenue In Interval
              numberOfUnitsInInterval = n())%>% # # Units In Interval
    
    mutate(accumulateBottom = cumsum(numberOfUnitsInInterval))%>% # CumulativeFromBottom
    arrange(desc(as.numeric(sub("\\%.*", "", interval))))%>% 
    mutate(accumulateTop = cumsum(numberOfUnitsInInterval))%>% # CumulativeFromTop
    arrange(as.numeric(sub("\\%.*", "", interval)))

  
  return(distribution.table)}

## 5. CutOff Strata List Function
CutStrata <- function(strataVar, revenueVar, revenue.summary, rev.share){

cut1 <- revenue.summary%>%
  filter(strataCount <= 10)%>%
  select(!!as.name(strataVar))

cut2 <- rev.share%>%
  group_by(!!as.name(strataVar))%>%
  mutate(index = order(!!as.name(revenueVar)))%>%
  filter(strataCount-index == 10,
         cumshare <= 0.1)%>%
  select(!!as.name(strataVar))

cut <- union(cut1 , cut2)

return(cut)

}



